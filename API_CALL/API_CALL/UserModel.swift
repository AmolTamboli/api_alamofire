//
//  UserModel.swift
//  API_CALL
//
//  Created by Amol Tamboli on 25/04/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import Foundation

struct Users: Codable {
    let body : String?
    let id : Int?
    let title : String?
    
    enum codingKeys : String,CodingKey {
        case body
        case id
        case title
    }
}

