//
//  ViewController.swift
//  API_CALL
//
//  Created by Amol Tamboli on 25/04/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    let URLStr = "https://jsonplaceholder.typicode.com/posts"
    var resultArray = [Users]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CallApi()
        // Do any additional setup after loading the view.
    }
 
    func CallApi(){
        let url = URL(string: URLStr)
        Alamofire.request(url!, method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            if let responseObj = response.value as? [[String:Any]] {
                self.resultArray = self.parsJSON(userData: responseObj)
                self.tblView.reloadData()
                print(self.resultArray)
            }
        }
    }
    
    func parsJSON(userData: [[String:Any]]) -> [Users] {
        var userArr = [Users]()
        for obj in userData{
            do {
                let data = try JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted)
                let decodeData = try JSONDecoder().decode(Users.self, from: data)
                userArr.append(decodeData)
            } catch {

            }
        }
        return userArr
    }
}
// MARK:- UITABLE VIEW METHOD

extension ViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = String(resultArray[indexPath.row].id!)
        cell.detailTextLabel?.text = resultArray[indexPath.row].title
        return cell
    }
    
}
